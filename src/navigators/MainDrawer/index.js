import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import HomeScreen from '../../pages/HomeScreen';
import AboutScreen from '../../pages/AboutScreen';
import ServicesScreen from '../../pages/ServicesScreen';
import ContactScreen from '../../pages/ContactScreen';

const Drawer = createDrawerNavigator();

const MainDrawer = () => {
  return (
    <Drawer.Navigator
      initialRouteName="Home"
      screenOptions={{
        drawerPosition: 'left',
        headerTitleAlign: 'center',
        headerTransparent: true,
        headerTitle: '',
        headerTintColor: '#FFF',
      }}
    >
      <Drawer.Screen name="Home" component={HomeScreen} />
      <Drawer.Screen name="About" component={AboutScreen} />
      <Drawer.Screen name="Services" component={ServicesScreen} />
      <Drawer.Screen name="Contact" component={ContactScreen} />
    </Drawer.Navigator>
  );
};

export default MainDrawer;
