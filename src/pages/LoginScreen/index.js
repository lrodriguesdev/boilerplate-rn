import React, { useState, useEffect } from 'react';

import { connect } from 'react-redux';

import { NavigationContainer } from '@react-navigation/native';
import MainDrawer from '../../navigators/MainDrawer';

import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { LoginManager, AccessToken } from 'react-native-fbsdk-next';

import FormLogin from '../../components/FormLogin';
import GoogleLogin from '../../components/GoogleLogin';
import FacebookLogin from '../../components/FacebookLogin';

import * as C from './styles';

const LoginScreen = (props) => {
  const [initializing, setInitializing] = useState(false);
  const [user, setUser] = useState();

  GoogleSignin.configure({
    webClientId:
      '648311811013-8d735a9ob4emvjbvn8t9e2g655kgj7r9.apps.googleusercontent.com',
  });

  if (user) {
    props.setName(user.displayName);
    props.setEmail(user.email);
  }

  const onGoogleButtonPress = async () => {
    try {
      const { idToken } = await GoogleSignin.signIn();
      const googleCredential = auth.GoogleAuthProvider.credential(idToken);

      return auth().signInWithCredential(googleCredential);
    } catch (error) {
      console.log(error);
    }
  };

  const onFacebookButtonPress = async () => {
    try {
      const result = await LoginManager.logInWithPermissions([
        'public_profile',
        'email',
      ]);

      if (result.isCancelled) {
        throw 'User cancelled the login process';
      }

      const data = await AccessToken.getCurrentAccessToken();

      if (!data) {
        throw 'Something went wrong obtaining access token';
      }

      const facebookCredential = auth.FacebookAuthProvider.credential(
        data.accessToken,
      );

      return auth().signInWithCredential(facebookCredential);
    } catch (error) {
      console.log(error);
    }
  };

  const onAuthStateChanged = (user) => {
    setUser(user);
    if (initializing) setInitializing(false);
  };

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber;
  }, []);

  if (initializing) return null;

  if (!user) {
    return (
      <C.Area>
        <C.ImageLogin
          resizeMode="contain"
          source={require('../../assets/login.png')}
        />

        <FormLogin />

        <C.AreaTextDivisor> Or </C.AreaTextDivisor>

        <GoogleLogin googleLogin={onGoogleButtonPress} />

        <FacebookLogin facebookLogin={onFacebookButtonPress} />
      </C.Area>
    );
  }
  return (
    <NavigationContainer>
      <MainDrawer />
    </NavigationContainer>
  );
};

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    setName: (name) => dispatch({ type: 'SET_NAME', payload: { name } }),
    setEmail: (email) => dispatch({ type: 'SET_EMAIL', payload: { email } }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
