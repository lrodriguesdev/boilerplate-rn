import styled from 'styled-components/native';

export const Area = styled.SafeAreaView`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const AreaTextDivisor = styled.Text`
  font-size: 15px;
  color: #7c4bd6;
  margin-bottom: 10px;
`;

export const ImageLogin = styled.Image`
  height: 200px;
`;
