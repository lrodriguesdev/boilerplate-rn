import React from 'react';
import * as C from './styles';

import {connect} from 'react-redux';

const ContactScreen = props => {
  return (
    <C.Area>
      <C.Info>Bem-vindo a tela Contact</C.Info>
      <C.Info>{props.name}</C.Info>
    </C.Area>
  );
};

const mapStateToProps = state => {
  return {
    name: state.userReducer.name,
    email: state.userReducer.email,
  };
};

export default connect(mapStateToProps)(ContactScreen);
