import styled from 'styled-components/native';

export const Area = styled.SafeAreaView`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #863b87;
`;

export const Info = styled.Text`
  font-size: 25px;
  color: #fff;
`;
