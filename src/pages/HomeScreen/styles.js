import styled from 'styled-components/native';

export const Area = styled.SafeAreaView`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #3b4187;
`;

export const Info = styled.Text`
  font-size: 25px;
  color: #fff;
`;

export const Button = styled.TouchableOpacity`
  margin-top: 25px;
  width: 80px;
  padding: 10px;
  background-color: #ff4545;
  border-radius: 5px;
  justify-content: center;
  align-items: center;
`;
