import React from 'react';
import auth from '@react-native-firebase/auth';
import * as C from './styles';

import { connect } from 'react-redux';

const HomeScreen = (props) => {
  const handleSair = async () => {
    await auth().currentUser?.delete();
  };

  return (
    <C.Area>
      <C.Info>Bem-vindo a tela Home</C.Info>
      <C.Info>{props.email}</C.Info>
      <C.Button onPress={handleSair}>
        <C.Info testID="button-sair">Sair</C.Info>
      </C.Button>
    </C.Area>
  );
};

const mapStateToProps = (state) => {
  return {
    name: state.userReducer.name,
    email: state.userReducer.email,
  };
};

export default connect(mapStateToProps)(HomeScreen);
