import styled from 'styled-components/native';

export const Area = styled.SafeAreaView`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: #3b8750;
`;

export const Info = styled.Text`
  font-size: 25px;
  color: #fff;
`;
