import React from 'react';
import * as C from './styles';

import {connect} from 'react-redux';

const AboutScreen = props => {
  return (
    <C.Area>
      <C.Info>Bem-vindo a tela About</C.Info>
      <C.Info>{props.name}</C.Info>
    </C.Area>
  );
};

const mapStateToProps = state => {
  return {
    name: state.userReducer.name,
    email: state.userReducer.email,
  };
};

export default connect(mapStateToProps)(AboutScreen);
