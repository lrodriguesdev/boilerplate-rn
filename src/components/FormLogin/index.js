import React from 'react';

import * as C from './styles';

import { Formik } from 'formik';
import * as yup from 'yup';

const loginValidationSchema = yup.object().shape({
  email: yup
    .string()
    .email('Please enter valid email')
    .required('Email address is required'),
  password: yup
    .string()
    .min(8, ({ min }) => `Password must be at leadt ${min} characters`)
    .required('Password is required')
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      'Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character',
    ),
});

const FormLogin = () => {
  return (
    <Formik
      initialValues={{ email: '', password: '' }}
      validateOnMount={true}
      onSubmit={(values) => console.log(values)}
      validationSchema={loginValidationSchema}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        touched,
        errors,
        isValid,
      }) => (
        <C.Area>
          <C.Input
            onChangeText={handleChange('email')}
            onBlur={handleBlur('email')}
            value={values.email}
            placeholder="exemple@email.com"
          />
          {errors.email && touched.email && <C.Erro>{errors.email}</C.Erro>}
          <C.Input
            onChangeText={handleChange('password')}
            onBlur={handleBlur('password')}
            value={values.password}
            secureTextEntry={true}
            placeholder="Entry your password"
          />
          {errors.password && touched.password && (
            <C.Erro>{errors.password}</C.Erro>
          )}
          <C.Button
            disabled={!isValid}
            onPress={handleSubmit}
            isValid={isValid}
          >
            <C.TextButton>Entrar</C.TextButton>
          </C.Button>
        </C.Area>
      )}
    </Formik>
  );
};

export default FormLogin;
