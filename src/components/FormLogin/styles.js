import styled from 'styled-components/native';

export const Area = styled.View`
  align-items: center;
  width: 280px;
`;

export const Input = styled.TextInput`
  border: 1px solid #ccc;
  width: 250px;
  height: 35px;
  padding: 8px;
  margin-bottom: 15px;
`;

export const Erro = styled.Text`
  font-size: 10px;
  color: red;
  margin-bottom: 15px;
`;

export const Button = styled.TouchableOpacity`
  background-color: ${(props) => (props.isValid ? '#7c4bd6' : '#919191')};
  width: 150px;
  padding: 5px;
  justify-content: center;
  align-items: center;
  margin-bottom: 15px;
`;

export const TextButton = styled.Text`
  color: #fff;
  font-size: 18px;
`;
