import styled from 'styled-components/native';

export const Button = styled.TouchableOpacity``;

export const TextButton = styled.Text`
  background-color: #4267b2;
  color: #fff;
  padding: 15px;
  width: 190px;
  text-align: center;
  font-weight: bold;
`;
