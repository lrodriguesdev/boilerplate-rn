import React from 'react';

import * as C from './styles';

const FacebookLogin = (props) => {
  return (
    <C.Button
      onPress={() =>
        props
          .facebookLogin()
          .then(() => console.log('Signed in with Facebook!'))
      }

      testID="button-facebook"
    >
      <C.TextButton>Sign in with Facebook</C.TextButton>
    </C.Button>
  );
};

export default FacebookLogin;
