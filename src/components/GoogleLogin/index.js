import React from 'react';
import { GoogleSigninButton } from '@react-native-google-signin/google-signin';

const GoogleLogin = (props) => {
  return (
    <GoogleSigninButton
      style={{ width: 192, height: 46, marginBottom: 10 }}
      size={GoogleSigninButton.Size.Wide}
      color={GoogleSigninButton.Color.Dark}
      testID="button-google"
      onPress={() =>
        props.googleLogin().then(() => console.log('Signed in with Google!'))
      }
    />
  );
};

export default GoogleLogin;
