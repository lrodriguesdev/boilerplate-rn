describe('My first suite tests E2E', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should have button facebook login', async () => {
    const info = await element(by.id('button-facebook'));
    await expect(info).toBeVisible();
  });
});
