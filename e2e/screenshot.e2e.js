/* eslint no-undef: 0  */
import { takeScreenshot } from './screenshot';
describe('screenshot', () => {
    beforeAll(async () => {
        await device.launchApp();
    });

    beforeEach(async () => {
        await device.reloadReactNative();
    });

  it('should take screenshots', async () => {
    takeScreenshot();
    await expect(element(by.id("button-google"))).toBeVisible();
    await element(by.id("button-google")).tap();

    await expect(element(by.id("button-sair"))).toBeVisible();
    takeScreenshot();
    await element(by.id("button-sair")).tap();
  });
});