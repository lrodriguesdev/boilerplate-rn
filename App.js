import React from 'react';

import { Provider } from 'react-redux';
import store from './src/store';

import codePush from 'react-native-code-push';
import LoginScreen from './src/pages/LoginScreen';

const App = () => {
  return (
    <Provider store={store}>
      <LoginScreen />
    </Provider>
  );
};

export default codePush({
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
})(App);
